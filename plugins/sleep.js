export default (ctx, inject) => {
  inject('sleep', ms => new Promise(resolve => setTimeout(resolve, ms)));
}
