import { v4 as uuid4 } from 'uuid';

export default (ctx, inject) => {
  inject('uuid', () => uuid4());
}
