export default (ctx, inject) => {
  inject('jsonClone', obj => JSON.parse(JSON.stringify(obj)));
}
