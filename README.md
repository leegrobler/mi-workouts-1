# MiWorkouts 1.0

**NOTE: This is not the offical [MiWorkouts](https://mi-workouts.com/), but its predecessor. MiWorkouts started life as a Nuxt.js PWA (this project), with the intention of having the user install it to his/her device and only having access to the his/her data locally.**

> My first real attempt at a marketable app and my first fully-functional PWA. MiWorkouts is a workout planner that allows you to keep track of your exercise plans and exercises details. The inspiration behind this project was to create an app that I could personally use in the gym, without excessive advertisements and features that I did not need. I wanted to extend this application to the public, so that they too could enjoy a workout application without being required to sign up and then pay to unlock basic features.

[Check out this project here](https://workout-planner-prod.herokuapp.com/) or [the official MiWorkouts, that includes cloud sync here](https://workout-planner-prod.herokuapp.com/).

## Run Locally

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```

#### Author

Lee Grobler  
hello@lee-grobler.com  

### Technologies Used

 - HTML
 - CSS
 - Javascript
 - Vue / Nuxt.js
 - PWA
 - Vuetify
 - Firebase
 - IndexedDb
 - Heroku
 - Bitbucket Pipelines
