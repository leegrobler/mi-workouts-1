import colors from 'vuetify/es5/util/colors';

export default {
  head: { // Global page headers: https://go.nuxtjs.dev/config-head
    title: `Workouts ${process.env.PSEUDO_ENV?.toUpperCase() || ''}`.trim(),
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'A to the point app that keeps track of your workouts' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  
  css: [ // Global CSS: https://go.nuxtjs.dev/config-css
    '@/assets/scss/animations.scss',
    '@/assets/scss/main.scss',
  ],

  plugins: [ // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    { src: '@/plugins/sleep.js', mode: 'client' },
    { src: '@/plugins/uuid.js', mode: 'client' },
    { src: '@/plugins/jsonClone.js', mode: 'client' },
  ],

  components: true, // Auto import components: https://go.nuxtjs.dev/config-components

  buildModules: [ // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    '@nuxtjs/vuetify', // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/moment',
    '@nuxtjs/localforage',
  ],

  modules: [ // Modules: https://go.nuxtjs.dev/config-modules
    '@nuxtjs/axios', // https://go.nuxtjs.dev/axios
    '@nuxtjs/pwa', // https://go.nuxtjs.dev/pwa
  ],

  localforage: {
    instances: [
      { name: 'nuxtJS', storeName: 'exercises' },
      { name: 'nuxtJS', storeName: 'routines' },
      { name: 'nuxtJS', storeName: 'plans' }
    ]
  },

  axios: {}, // Axios module configuration: https://go.nuxtjs.dev/config-axios

  pwa: { // PWA module configuration: https://go.nuxtjs.dev/pwa
    manifest: {
      name: `Workouts ${process.env.PSEUDO_ENV?.toUpperCase() || ''}`.trim(),
      short_name: `Workouts ${process.env.PSEUDO_ENV?.toUpperCase() || ''}`.trim(),
      lang: 'en',
    }
  },

  vuetify: { // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
    customVariables: ['~/assets/scss/variables.scss'],
    treeshake: true,
    theme: {
      dark: false,
      options: { customProperties: true },
      themes: {
        dark: {
          primary: colors.blue.darken2,
          secondary: colors.amber.darken3,
          accent: colors.grey.darken3,
          info: colors.blue.base,
          warning: colors.orange.darken1,
          error: colors.red.accent2,
          success: colors.green.base,
        },
        light: {
          primary: colors.blue.darken2,
          secondary: colors.amber.darken3,
          accent: colors.grey.darken3,
          info: colors.blue.base,
          warning: colors.orange.darken1,
          error: colors.red.accent2,
          success: colors.green.base,
        }
      }
    }
  },

  build: {}, // Build Configuration: https://go.nuxtjs.dev/config-build

  env: {
    PSEUDO_ENV: process.env.PSEUDO_ENV,
  },
}
