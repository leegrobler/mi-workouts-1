export const state = () => ({
  flashValue: null, // the idea with the flash value is that it should be cleared immediately after using it, so please do that
});

export const getters = {
  getFlashValue: state => state.flashValue,
};

export const mutations = {
  setFlashValue: (state, payload) => state.flashValue = payload,
};

export const actions = {
  setFlashValue({ commit }, payload) {
    commit('setFlashValue', payload);
  },
};
