export const state = () => ({
  plans: [],
});

export const getters = {
  getPlans: (state, getters, rootState, rootGetters) => {
    return JSON.parse(JSON.stringify(state.plans)).map(v => {
      v.routines = v.routines.map(v2 => rootGetters['routine/getRoutines'].find(v3 => v3.id === v2));
      return v;
    }).sort((a, b) => a.name > b.name ? 1 : -1);
  },
};

export const mutations = {
  setPlans: (state, payload) => state.plans = payload,
};

export const actions = {
  fetchPlans({ commit }, payload) {
    return new Promise(async (resolve, reject) => {
      try {
        const keys = await this.$localForage.plans.keys();
        const plans = [];

        for(let i = 0; i < keys.length; i++) {
          const plan = await this.$localForage.plans.getItem(keys[i]);
          plans.push(plan);
        }

        commit('setPlans', plans);
        return resolve(plans);
      } catch (err) {
        return reject(err);
      }
    });
  },

  fetchPlan({ dispatch }, payload) {
    return new Promise(async (resolve, reject) => {
      try {
        const plans = await dispatch('fetchPlans');
        const ret = plans.find(v => v[payload.prop] === payload.value);
        return resolve(ret);
      } catch (err) {
        return reject(err);
      }
    });
  },

  upsertPlan({ dispatch }, payload) {
    return new Promise(async (resolve, reject) => {
      try {
        const plan = payload.id ? payload : { id: this.$uuid(), ...payload };
        plan.routines = plan.routines.map(v => typeof v === 'string' ? v : v.id);

        await this.$localForage.plans.setItem(plan.id, plan);
        await dispatch('fetchPlans');

        return resolve(plan);
      } catch (err) {
        return reject(err);
      }
    });
  },
  
  deletePlan({ dispatch }, payload) {
    return new Promise(async (resolve, reject) => {
      try {
        await this.$localForage.plans.removeItem(payload.id);
        await dispatch('fetchPlans');
        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  },
};
