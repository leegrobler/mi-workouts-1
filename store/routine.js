export const state = () => ({
  routines: [],
});

export const getters = {
  getRoutines: (state, getters, rootState, rootGetters) => {
    return JSON.parse(JSON.stringify(state.routines)).map(v => {
      v.exercises = v.exercises.map(v2 => rootGetters['exercise/getExercises'].find(v3 => v3.id === v2));
      return v;
    }).sort((a, b) => a.name > b.name ? 1 : -1);
  },
};

export const mutations = {
  setRoutines: (state, payload) => state.routines = payload,
};

export const actions = {
  fetchRoutines({ commit }, payload) {
    return new Promise(async (resolve, reject) => {
      try {
        const keys = await this.$localForage.routines.keys();
        const routines = [];

        for(let i = 0; i < keys.length; i++) {
          const routine = await this.$localForage.routines.getItem(keys[i]);
          routines.push(routine);
        }

        commit('setRoutines', routines);
        return resolve(routines);
      } catch (err) {
        return reject(err);
      }
    });
  },

  fetchRoutine({ dispatch }, payload) {
    return new Promise(async (resolve, reject) => {
      try {
        const routines = await dispatch('fetchRoutines');
        const ret = routines.find(v => v[payload.prop] === payload.value);
        return resolve(ret);
      } catch (err) {
        return reject(err);
      }
    });
  },

  upsertRoutine({ dispatch }, payload) {
    return new Promise(async (resolve, reject) => {
      try {
        const routine = payload.id ? payload : { id: this.$uuid(), ...payload };
        routine.exercises = routine.exercises.map(v => typeof v === 'string' ? v : v.id);

        await this.$localForage.routines.setItem(routine.id, routine);
        await dispatch('fetchRoutines');

        return resolve(routine);
      } catch (err) {
        return reject(err);
      }
    });
  },
  
  deleteRoutine({ dispatch }, payload) {
    return new Promise(async (resolve, reject) => {
      try {
        await this.$localForage.routines.removeItem(payload.id);
        await dispatch('fetchRoutines');
        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  },
};
