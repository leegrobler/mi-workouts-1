export const state = () => ({
  exercises: [],
});

export const getters = {
  getExercises: state => JSON.parse(JSON.stringify(state.exercises)).sort((a, b) => a.name > b.name ? 1 : -1),
};

export const mutations = {
  setExercises: (state, payload) => state.exercises = payload,
};

export const actions = {
  fetchExercises({ commit }, payload) {
    return new Promise(async (resolve, reject) => {
      try {
        const keys = await this.$localForage.exercises.keys();
        const exercises = [];

        for(let i = 0; i < keys.length; i++) {
          const exercise = await this.$localForage.exercises.getItem(keys[i]);
          exercises.push(exercise);
        }

        commit('setExercises', exercises);
        return resolve(exercises);
      } catch (err) {
        return reject(err);
      }
    });
  },

  fetchExercise({ dispatch }, payload) {
    return new Promise(async (resolve, reject) => {
      try {
        const exercises = await dispatch('fetchExercises');
        const ret = exercises.find(v => v[payload.prop] === payload.value);
        return resolve(ret);
      } catch (err) {
        return reject(err);
      }
    });
  },

  upsertExercise({ dispatch }, payload) {
    return new Promise(async (resolve, reject) => {
      try {
        let exercise = await dispatch('fetchExercise', { prop: 'name', value: payload.name });
        if(exercise && exercise.id !== payload.id) return reject({ message: `${payload.name} already exists` });
        exercise = payload.id ? payload : { id: this.$uuid(), ...payload };

        await this.$localForage.exercises.setItem(exercise.id, exercise);
        await dispatch('fetchExercises');

        return resolve(exercise);
      } catch (err) {
        return reject(err);
      }
    });
  },

  deleteExercise({ dispatch }, payload) {
    return new Promise(async (resolve, reject) => {
      try {
        await this.$localForage.exercises.removeItem(payload.id);
        await dispatch('fetchExercises');
        return resolve();
      } catch (err) {
        return reject(err);
      }
    });
  },
};
