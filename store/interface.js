export const state = () => ({
  alert: {},
  dialogue: {},
});

export const getters = {
  getAlert: state => state.alert,
  getDialogue: state => state.dialogue,
};

export const mutations = {
  setAlert: (state, payload) => state.alert = payload,
  showAlert: (state, payload) => state.alert = { visible: true, ...payload },
  hideAlert: state => state.alert = {},
  showDialogue: (state, payload) => state.dialogue = { visible: true, ...payload },
  hideDialogue: state => state.dialogue = {},
};

export const actions = {};
