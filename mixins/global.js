import { mapMutations } from 'vuex';

export default {
  methods: {
    ...mapMutations({
      revealAlert: 'interface/showAlert',
      revealDialogue: 'interface/showDialogue',
    }),
    alert(cfg) {
      this.revealAlert(cfg);
    },
    dialogue(cfg) {
      this.revealDialogue(cfg);
    },
  },
};
